# SurvivalPong

SurvivalPong, ported to the Bevy Engine!

Originally made by DeavidSedice on the Macroquad Game Engine as part of his Rust Tutorial for beginners, it has now been rewritten from the ground up for a Game Engine making use of an Entity Component System.

<p align="center">
<img src="https://codeberg.org/Olhi/survival_pong/raw/branch/main/assets/SurvivalPong.png"></a>
</p>

**Controls:**

```W``` and ```UP``` to move Paddle up.    
```S``` and ```DOWN``` to move Paddle Down.    
```R``` to restart the Game.    
```F4``` to trigger the Debug Mode.

# Compile the Game:

If you haven't already, install Rust: https://www.rust-lang.org/tools/install

Clone the repository:

```git clone https://codeberg.org/Olhi/survival_pong.git```

Change the working directory:

```cd survival_pong```

Compile and run the game using cargo:

```cargo run --release```

# License:

This game is free/open-source software released under GPLv3. See [```LICENSE.txt```](https://codeberg.org/Olhi/survival_pong/src/branch/main/LICENSE.txt).

# Credits:

SurvivalPong by **Olhi**

Original *Macroquad* Game by **DeavidSedice** (licensed under CC-BY 4.0):    

https://github.com/deavid/lprfl/tree/main/projects/survivalpong    
https://github.com/deavid/lprfl/blob/main/LICENSE-CC-BY    
https://deavid.github.io/lprfl/L03apprentice/020A_proj_game_macroquad.html    

Font is "*Press Start 2P*" by **codeman38**: https://www.fontspace.com/press-start-2p-font-f11591