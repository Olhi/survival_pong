use bevy::core::FixedTimestep;
use bevy::diagnostic::FrameTimeDiagnosticsPlugin;
use bevy::prelude::*;

const TRANSPARENT: Color = Color::rgba(0.0, 0.0, 0.0, 0.0);
const BLUE: Color = Color::rgb(102.0 / 255.0, 191.0 / 255.0, 1.0);
const PADDLE_POS_X: f32 = -350.0;
const PADDLE_SIZE_X: f32 = 10.0;
const BALL_SIZE: f32 = 10.0;
const SPEED_MULT: f32 = 1.1;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.04, 0.04, 0.04)))
        .insert_resource(WindowDescriptor {
            title: "Survival Pong!".to_string(),
            width: 800.0,
            height: 800.0,
            resizable: false,
            ..default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(FrameTimeDiagnosticsPlugin)
        .add_startup_system(setup_camera)
        .add_startup_system(spawn_entities)
        .add_startup_system(debug_field)
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(1.0 / 25.0))
                .with_system(debug_text),
        )
        .add_system(debug_trigger)
        .add_system(ball_movement.before(ball_events))
        .add_system(paddle_movement.before(boundaries))
        .add_system(boundaries)
        .add_system(ball_events)
        .add_system(fps_display)
        .add_system(game_over)
        .add_event::<GameOverEvent>()
        .run();
}
#[derive(Component)]
struct Paddle;

#[derive(Component)]
struct Ball;

#[derive(Component, Clone, Copy)]
struct Momentum {
    x: f32,
    y: f32,
}

#[derive(Component)]
struct Debug;

#[derive(Component)]
struct FrameRate;

#[derive(Component)]
struct Score;

struct GameOverEvent;

fn setup_camera(mut commands: Commands, asset_server: Res<AssetServer>) {
    // Load the Font: https://www.fontspace.com/press-start-2p-font-f11591
    let font = asset_server.load("PressStart2P-vaV7.ttf");
    // Load the Boder Picture (Green Lines):
    let field: Handle<Image> = asset_server.load("Playfield.png");

    commands.spawn_bundle(OrthographicCameraBundle::new_2d());

    // SurvivalPong Logo Display:
    commands.spawn_bundle(Text2dBundle {
        text: Text::with_section(
            "SurvivalPong",
            TextStyle {
                font: font.clone(),
                font_size: 60.0,
                color: Color::rgb(128.0 / 255.0, 128.0 / 255.0, 128.0 / 255.0),
            },
            TextAlignment {
                vertical: VerticalAlign::Center,
                horizontal: HorizontalAlign::Center,
            },
        ),
        transform: Transform::from_xyz(-5.0, -345.0, 0.0),
        ..default()
    });
    commands.spawn_bundle(Text2dBundle {
        text: Text::with_section(
            "SurvivalPong",
            TextStyle {
                font: font.clone(),
                font_size: 60.0,
                color: Color::rgb(192.0 / 255.0, 192.0 / 255.0, 192.0 / 255.0),
            },
            TextAlignment {
                vertical: VerticalAlign::Center,
                horizontal: HorizontalAlign::Center,
            },
        ),
        transform: Transform::from_xyz(0.0, -350.0, 50.0),
        ..default()
    });
    // FPS Counter:
    commands
        .spawn_bundle(Text2dBundle {
            text: Text::with_section(
                0.0.to_string(),
                TextStyle {
                    font: font.clone(),
                    font_size: 30.0,
                    color: Color::GREEN,
                },
                TextAlignment {
                    vertical: VerticalAlign::Center,
                    horizontal: HorizontalAlign::Center,
                },
            ),
            transform: Transform::from_xyz(300.0, 240.0, 50.0),
            ..default()
        })
        .insert(FrameRate);

    // SCORE Display:
    commands
        .spawn_bundle(Text2dBundle {
            text: Text::with_section(
                "00000",
                TextStyle {
                    font: font.clone(),
                    font_size: 30.0,
                    color: BLUE,
                },
                TextAlignment {
                    vertical: VerticalAlign::Center,
                    horizontal: HorizontalAlign::Center,
                },
            ),
            transform: Transform::from_xyz(-298.0, 240.0, 50.0),
            ..default()
        })
        .insert(Score);

    commands.spawn_bundle(SpriteBundle {
        texture: field,
        ..default()
    });
}

// Spawn Paddle & Ball. Gets restarted after a Game Over.
fn spawn_entities(mut commands: Commands) {
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::YELLOW,
                ..default()
            },
            transform: Transform {
                scale: Vec3::new(PADDLE_SIZE_X, 150.0, 00.0),
                translation: Vec3::new(PADDLE_POS_X, 0.0, 00.0),
                ..default()
            },
            ..default()
        })
        .insert(Paddle);
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: BLUE,
                ..default()
            },
            transform: Transform {
                scale: Vec3::new(BALL_SIZE, BALL_SIZE, 10.0),
                translation: Vec3::new(300.0, 0.0, 10.0),
                ..default()
            },
            ..default()
        })
        .insert(Ball)
        .insert(Momentum { x: 5.0, y: 5.0 });
}

// Paddle Movement triggered by Keyboard Inputs.
fn paddle_movement(
    keyboard_input: Res<Input<KeyCode>>,
    mut paddle_values: Query<&mut Transform, (With<Paddle>, Without<Ball>)>,
    mut game_over: EventWriter<GameOverEvent>,
) {
    for mut transform in paddle_values.iter_mut() {
        if keyboard_input.pressed(KeyCode::Up) || keyboard_input.pressed(KeyCode::W) {
            transform.translation.y += 10.0;
        }
        if keyboard_input.pressed(KeyCode::Down) || keyboard_input.pressed(KeyCode::S) {
            transform.translation.y -= 10.0;
        };
    }
    if keyboard_input.just_pressed(KeyCode::R) {
        game_over.send(GameOverEvent);
    }
}

// Ball Movement. Initial Speed is 5x5.
fn ball_movement(mut ball_values: Query<(&mut Transform, &Momentum), With<Ball>>) {
    for (mut ball, momentum) in ball_values.iter_mut() {
        ball.translation.y -= momentum.y;
        ball.translation.x -= momentum.x;
    }
}

// Ensures that Paddle will stop at Wall regardless of size.
// paddle.translation.y = Paddle Position
// paddle.scale.y = Paddle Size (Default 150.0, can shrink over time.)
fn boundaries(mut paddle_values: Query<&mut Transform, With<Paddle>>) {
    for mut paddle in paddle_values.iter_mut() {
        if paddle.translation.y + paddle.scale.y / 2.0 > 223.0 {
            paddle.translation.y = 223.0 - paddle.scale.y / 2.0
        };
        if paddle.translation.y - paddle.scale.y / 2.0 < -224.0 {
            paddle.translation.y = -224.0 + paddle.scale.y / 2.0
        };
    }
}

// Make Ball invert momentum when hitting a Wall.
// Make Ball shrink Paddle upon hitting it.
// Increase Score when Ball hits a Wall.
// Trigger Game Over when Call goes past the goal area.
// Play sounds to indicate Ball Events.
fn ball_events(
    asset_server: Res<AssetServer>,
    audio: Res<Audio>,
    mut ball_values: Query<(&mut Transform, &mut Momentum), (With<Ball>, Without<Paddle>)>,
    mut paddle_values: Query<&mut Transform, (With<Paddle>, Without<Ball>)>,
    mut text_value: Query<&mut Text, With<Score>>,
    mut game_over: EventWriter<GameOverEvent>,
) {
    let over = asset_server.load("over.ogg");
    let click = asset_server.load("click.ogg");
    let snare = asset_server.load("snare.wav");
    for (mut ball, mut momentum) in ball_values.iter_mut() {
        for mut text in text_value.iter_mut() {
            let score_update = format!(
                "{:05}",
                (text.sections[0].value.parse::<u16>().unwrap() + 1)
            );
            if ball.translation.y > 219.0 {
                ball.translation.y = 218.0;
                momentum.y *= -1.0;
                audio.play(click.clone());
                text.sections[0].value = score_update.clone()
            };
            if ball.translation.y < -219.0 {
                ball.translation.y = -218.0;
                momentum.y *= -1.0;
                audio.play(click.clone());
                text.sections[0].value = score_update.clone()
            };
            if ball.translation.x > 370.0 {
                ball.translation.x = 369.0;
                momentum.x *= -1.0;
                audio.play(click.clone());
                text.sections[0].value = score_update.clone()
            };
            for mut paddle in paddle_values.iter_mut() {
                // The following section is copied & adapted from Deavid's Original.
                // DOES BALL HIT PADDLE?
                if ball.translation.x - BALL_SIZE / 2.0 < PADDLE_POS_X + PADDLE_SIZE_X / 2.0
                    && ball.translation.x + BALL_SIZE / 2.0 > PADDLE_POS_X - PADDLE_SIZE_X / 2.0
                {
                    // In the same X.
                    if ball.translation.y - BALL_SIZE / 2.0
                        < paddle.translation.y + paddle.scale.y / 2.0
                        && ball.translation.y + BALL_SIZE / 2.0
                            > paddle.translation.y - paddle.scale.y / 2.0
                    {
                        // In the same Y
                        if momentum.x > 0.0 {
                            momentum.x *= -1.0;
                            let speed =
                                f32::sqrt(momentum.x * momentum.x + momentum.y * momentum.y)
                                    * SPEED_MULT;
                            let diff_y =
                                (ball.translation.y - paddle.translation.y) / paddle.scale.y / 2.0;
                            momentum.y += diff_y * 5.0;
                            let speed2 =
                                f32::sqrt(momentum.x * momentum.x + momentum.y * momentum.y);
                            let f = (speed / speed2).sqrt();
                            momentum.x *= f;
                            momentum.y *= f;
                            text.sections[0].value = score_update.clone();
                            paddle.scale.y /= 1.05;
                            audio.play(snare.clone());
                        }
                    }
                }
            }
            if ball.translation.x < -370.0 {
                {
                    audio.play(over.clone());
                    game_over.send(GameOverEvent);
                }
            };
        }
    }
}

// Determines Framerate via the FrameTimeDiagnosticsPlugin.
// See https://github.com/bevyengine/bevy/blob/v0.7.0/examples/ui/text_debug.rs
fn fps_display(
    diagnostics: Res<bevy::diagnostic::Diagnostics>,
    mut text_value: Query<&mut Text, With<FrameRate>>,
) {
    for mut text in text_value.iter_mut() {
        let mut fps = 0.0;
        if let Some(fps_diagnostic) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
            if let Some(fps_avg) = fps_diagnostic.average() {
                fps = fps_avg;
            }
        }
        text.sections[0].value = format!("{:.2}", fps)
    }
}

// Despawns Paddle+Ball, resets Score, respawns Paddle+Ball.
fn game_over(
    mut commands: Commands,
    mut reader: EventReader<GameOverEvent>,
    mut text_value: Query<&mut Text, With<Score>>,
    paddle: Query<Entity, With<Paddle>>,
    ball: Query<Entity, With<Ball>>,
) {
    if reader.iter().next().is_some() {
        for ent in ball.iter().chain(paddle.iter()) {
            commands.entity(ent).despawn();
        }
        for mut text in text_value.iter_mut() {
            text.sections[0].value = format!("{:05}", (0))
        }
        spawn_entities(commands);
    }
}

// Generates four purple lines on top of the Playfield for the sake of coordination.
// Generates 6 Text Sections displaying Debug Stuff.
// Press F4 to enable/disable.

fn debug_field(asset_server: Res<AssetServer>, mut commands: Commands) {
    // Four Purple Lines.
    commands.spawn_bundle(UiCameraBundle::default());

    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: TRANSPARENT,
                ..default()
            },
            transform: Transform {
                scale: Vec3::new(2.0, 800.0, 10.0),
                translation: Vec3::new(374.0, 0.0, 10.0),
                ..default()
            },
            ..default()
        })
        .insert(Debug);
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: TRANSPARENT,
                ..default()
            },
            transform: Transform {
                scale: Vec3::new(800.0, 2.0, 10.0),
                translation: Vec3::new(0.0, 224.0, 10.0),
                ..default()
            },
            ..default()
        })
        .insert(Debug);
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: TRANSPARENT,
                ..default()
            },
            transform: Transform {
                scale: Vec3::new(800.0, 2.0, 10.0),
                translation: Vec3::new(0.0, -224.0, 10.0),
                ..default()
            },
            ..default()
        })
        .insert(Debug);
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: TRANSPARENT,
                ..default()
            },
            transform: Transform {
                scale: Vec3::new(2.0, 800.0, 10.0),
                translation: Vec3::new(-374.0, 0.0, 10.0),
                ..default()
            },
            ..default()
        })
        .insert(Debug);

    // Debug Text Sections.
    let font = asset_server.load("PressStart2P-vaV7.ttf");
    let font_size: f32 = 20.0;
    commands
        .spawn_bundle(TextBundle {
            style: Style {
                align_self: AlignSelf::FlexEnd,
                position_type: PositionType::Relative,
                position: Rect {
                    top: Val::Px(3.0),
                    left: Val::Px(31.0),
                    ..default()
                },
                ..default()
            },
            text: Text {
                sections: vec![
                    TextSection {
                        value: "Ball Speed:             ###px/f".to_string(),
                        style: TextStyle {
                            font: font.clone(),
                            font_size: font_size,
                            color: TRANSPARENT,
                        },
                    },
                    TextSection {
                        value: "\nHorizontal Position:              ###".to_string(),
                        style: TextStyle {
                            font: font.clone(),
                            font_size: font_size,
                            color: TRANSPARENT,
                        },
                    },
                    TextSection {
                        value: "\nVertical Position:                ###".to_string(),
                        style: TextStyle {
                            font: font.clone(),
                            font_size: font_size,
                            color: TRANSPARENT,
                        },
                    },
                    TextSection {
                        value: "\nPaddle Size:                      ###".to_string(),
                        style: TextStyle {
                            font: font.clone(),
                            font_size: font_size,
                            color: TRANSPARENT,
                        },
                    },
                    TextSection {
                        value: "\nPaddle Position:                  ###".to_string(),
                        style: TextStyle {
                            font: font.clone(),
                            font_size: font_size,
                            color: TRANSPARENT,
                        },
                    },
                    TextSection {
                        value: "\nPress the F4-Key to hide the Debug.".to_string(),
                        style: TextStyle {
                            font: font.clone(),
                            font_size: font_size,
                            color: TRANSPARENT,
                        },
                    },
                ],
                alignment: TextAlignment {
                    vertical: VerticalAlign::Top,
                    horizontal: HorizontalAlign::Left,
                },
            },
            ..default()
        })
        .insert(Debug);
}

// Updates the Text Sections with the corresponding values.
fn debug_text(
    mut ball_values: Query<(&Transform, &Momentum), (With<Ball>, Without<Paddle>)>,
    mut paddle_values: Query<&Transform, (With<Paddle>, Without<Ball>)>,
    mut debug: Query<&mut Text, With<Debug>>,
) {
    for (ball, momentum) in ball_values.iter_mut() {
        let speed: f32 = (momentum.x * momentum.x + momentum.y * momentum.y).sqrt() * 60.0;

        for mut ball_debug in debug.iter_mut() {
            ball_debug.sections[0].value = format!(
                "\nBall Speed:{num: >wid$}px/s",
                num = speed as i32,
                wid = 22
            );
        }
        let ball_x: f32 = ball.translation.x;
        let ball_y: f32 = ball.translation.y;

        for mut ball_debug in debug.iter_mut() {
            ball_debug.sections[1].value = format!(
                "\nHorizontal Position:{num: >wid$}",
                num = ball_x as i32,
                wid = 17
            );
            ball_debug.sections[2].value = format!(
                "\nVertical Position:{num: >wid$}",
                num = ball_y as i32,
                wid = 19
            );
        }
    }

    for paddle in paddle_values.iter_mut() {
        let paddle_pos: f32 = paddle.translation.y;
        let paddle_size: f32 = paddle.scale.y;
        {
            for mut paddle_debug in debug.iter_mut() {
                paddle_debug.sections[4].value = format!(
                    "\nPaddle Position:{num: >wid$}",
                    num = paddle_pos as i32,
                    wid = 21
                );
                paddle_debug.sections[3].value = format!(
                    "\nPaddle Size:{num: >wid$}",
                    num = paddle_size as i32,
                    wid = 25
                );
            }
        }
    }
}

// By default, the Debug Display is running from the start,
// but hidden through a transparent color. This is done to
// prevent slow loads that would occur when spawning the
// Debug Entities through a keypress.
fn debug_trigger(
    keyboard_input: Res<Input<KeyCode>>,
    mut border_color: Query<&mut Sprite, With<Debug>>,
    mut debug: Query<&mut Text, With<Debug>>,
) {
    for mut sprite in border_color.iter_mut() {
        for mut color_debug in debug.iter_mut() {
            if keyboard_input.just_pressed(KeyCode::F4) {
                if sprite.color == TRANSPARENT {
                    color_debug.sections[0].style.color = Color::RED;
                    color_debug.sections[1].style.color = Color::ORANGE;
                    color_debug.sections[2].style.color = Color::ORANGE;
                    color_debug.sections[3].style.color = Color::YELLOW;
                    color_debug.sections[4].style.color = Color::YELLOW;
                    color_debug.sections[5].style.color = Color::SILVER;
                    sprite.color = Color::PURPLE;
                } else if sprite.color == Color::PURPLE {
                    color_debug.sections[0].style.color = TRANSPARENT;
                    color_debug.sections[1].style.color = TRANSPARENT;
                    color_debug.sections[2].style.color = TRANSPARENT;
                    color_debug.sections[3].style.color = TRANSPARENT;
                    color_debug.sections[4].style.color = TRANSPARENT;
                    color_debug.sections[5].style.color = TRANSPARENT;
                    sprite.color = TRANSPARENT;
                }
            }
        }
    }
}
